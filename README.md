# README #

### What is this repository for? ###

* The aim of the project is to collect statistics for CS:GO.
* 0.0.1

### How do I get set up? ###

# Summary of set up: #
To get started it is enough to clone the project and build it using maven. Build has to be done using the main pom.xml. 

* Configuration - Coming soon
* Dependencies - Coming soon
* Database configuration - Coming soon
* How to run tests - Coming soon
# Deployment instructions: #
In the project Tomcat 8 is used. On tomcat has to be deployed statMaster-rest.war.

Rest End Points - Coming soon

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* danielzaru@gmail.com

### To-Do List ###

* Add JavaDoc
* Create better database structure (Java entites as well) for security, it means to develop user roles and think about user entity in more detail
* Implement tests for all services
* Integrate already developed part of the frontend to a newer version of the backend