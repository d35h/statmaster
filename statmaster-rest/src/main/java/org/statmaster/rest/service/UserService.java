package org.statmaster.rest.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.statmaster.rest.entity.User;

public interface UserService extends JpaRepository<User, Integer>  {
	
}