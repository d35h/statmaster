package org.statmaster.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.statmaster.rest.entity.User;
import org.statmaster.rest.service.UserService;

@Service
public class UserServiceImpl {
	
	@Autowired
	private UserService userRepository;
	
	@Transactional
	public User getUserById(Integer userId) {
		System.out.println("Getting user from the database by id: " + userId);
		
		return userRepository.findOne(userId);
	}
	
}
