package org.statmaster.rest.jpa.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.statmaster.rest.service")
@PropertySource("classpath:app.properties")
public class JPAJavaConfig {

	private final static String ENTITY_SCAN_PACKAGES = "org.statmaster.rest.entity";
	
	private final static String DATASOURCE_DRIVER = "spring.datasource.driver";
	private final static String DATASOURCE_URL = "spring.datasource.url";
	private final static String DATASOURCE_USERNAME = "spring.datasource.username";
	private final static String DATASOURCE_PASSWORD = "spring.datasource.password";
	
	private final static String SPRING_JPA_DIALECT = "spring.jpa.dialect";
	
	@Autowired
	private Environment env;
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { ENTITY_SCAN_PACKAGES });

		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}

	@Bean
	public DataSource dataSource() {
		HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName(env.getProperty(DATASOURCE_DRIVER));
        dataSourceConfig.setJdbcUrl(env.getProperty(DATASOURCE_URL));
        dataSourceConfig.setUsername(env.getProperty(DATASOURCE_USERNAME));
        dataSourceConfig.setPassword(env.getProperty(DATASOURCE_PASSWORD));
		
		return new HikariDataSource(dataSourceConfig);
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", env.getProperty(SPRING_JPA_DIALECT));
		
		return properties;
	}

}