package org.statmaster.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.statmaster.rest.entity.User;
import org.statmaster.rest.service.impl.UserServiceImpl;


/**
 * Handles requests for the application home page.
 */
@RestController
public class HomeController {
	
	private final static Integer HARDCODED_USER_ID = 1;
	
	@Autowired
	UserServiceImpl userService;
	
	@ResponseBody
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public User test() {
		return userService.getUserById(HARDCODED_USER_ID);
	}
	
	
}
